module Day8
    ( main
    ) where

import Data.List
import Data.Ord (comparing)

main :: IO ()
main = do
    puzzleInput <- readFile "input/input_8.txt"
    let pixels = head $ lines puzzleInput
        rows = chunkBy 25 pixels
        layers = chunkBy 6 rows
        least = leastZeros layers
        part1 = tally '1' least * tally '2' least
    print least
    print part1

-- initial implementation had stack overflow
-- idea for this refactoring from lynn's example on github
-- was actually because of a dumb bug i didn't notice so i cheated for no reason
-- :^(

chunkBy :: Int -> [a] -> [[a]]
chunkBy _ [] = []
chunkBy n list = take n list : chunkBy n (drop n list)

tally :: Char -> [String] -> Int
tally c = length . filter (c==) . concat

leastZeros :: [[String]] -> [String]
leastZeros = minimumBy (comparing (tally '0'))

-- part 2

black :: Char
black = '💮'

white :: Char
white = '🌹'

-- study until you can work it out yourself, dumpass
-- using ZipWith porbably
