module Day4
    ( main
    ) where

main :: IO ()
main = do
    print $ length [x | x <- range, doubleTest x, decTest x]
    print $ length [x | x <- range, loneDoubleTest x, decTest x]

range :: [Int]
range = [138241 .. 674034]

checkInt :: (String -> Bool) -> Int -> Bool
checkInt f n = f $ show n

hasDouble :: Eq a => [a] -> Bool
hasDouble (x:y:xs)
  | x == y    = True
  | otherwise = hasDouble (y:xs)
hasDouble _ = False

hasLoneDouble :: Eq a => [a] -> Bool
hasLoneDouble = go []
    where go memory (x:xs) =
            if x `notElem` memory 
              then length memory == 2 || go [x] xs
              else go (x:memory) xs
          go memory [] = length memory == 2


noDec :: Ord a => [a] -> Bool
noDec (x:y:xs)
  | x > y = False
  | otherwise = noDec (y:xs)
noDec _ = True

doubleTest :: Int -> Bool
doubleTest = checkInt hasDouble

loneDoubleTest :: Int -> Bool
loneDoubleTest = checkInt hasLoneDouble

decTest :: Int -> Bool
decTest = checkInt noDec
