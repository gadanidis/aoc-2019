module Day5
    ( main
    ) where

import Data.List.Split (splitOn)

main :: IO ()
main = do
    puzzleInput <- readFile "input/input_5.txt"
    let program = map read (splitOn "," puzzleInput) :: [Int]

    putStr "hey: "
    line <- getLine
    let input = read line :: Int

    let output = runIt input program

    print output

type Program = [Int]

data Opcode = Arithmetic (Int -> Int -> Int)
            | Scan
            | Print
            | Halt
            | JumpIf (Int -> Bool)

type Input = Int

type Output = Int

data Mode = Position
          | Immediate

readOpstring :: Int -> (Opcode, [Mode])
readOpstring n = case length (digits n :: [Int]) of
                   1 -> (readOpcode op, replicate 3 Position)
                   2 -> (readOpcode op, replicate 3 Position)
                   3 -> (readOpcode op, readMode (head mo) : replicate 2 Position)
                   4 -> (readOpcode op, map readMode mo ++ [Position])
                   5 -> (readOpcode op, map readMode mo)
                where op = rem n 100
                      mo = drop 2 $ reverse $ digits n
                 

readMode :: Int -> Mode
readMode n = case n of
               0 -> Position
               1 -> Immediate
               e -> error $ "bad mode: " ++ show e

readOpcode :: Int -> Opcode
readOpcode n = case n of
                1 -> Arithmetic (+)
                2 -> Arithmetic (*)
                3 -> Scan
                4 -> Print
                5 -> JumpIf (/= 0)
                6 -> JumpIf (== 0)
                7 -> Arithmetic (\x y -> if x < y then 1 else 0)
                8 -> Arithmetic (\x y -> if x == y then 1 else 0)
                99 -> Halt
                e -> error $ "bad opcode: " ++ show e

runIt :: Input -> Program -> (Program, [Output])
runIt input prg = runProgram input 0 (prg, [])

runProgram :: Input -> Int -> (Program, [Output]) -> (Program, [Output])
runProgram input pos prg =
    let program = fst prg
        output = snd prg
        code  = fst $ readOpstring (program!!pos)
        modes = snd $ readOpstring (program!!pos)
        getValue Position index = program!!(program!!index)
        getValue Immediate index = (program!!index)
     in
        case code of
          Arithmetic f -> runProgram input (pos + 4) (swap out res program, output)
              where out = program!!(pos+3)
                    in1 = getValue (head modes) (pos+1)
                    in2 = getValue (modes!!1) (pos+2)
                    res = in1 `f` in2
          Scan -> runProgram input (pos + 2) (swap out input program, output)
              where out = getValue (head modes) pos
          Print -> runProgram input (pos + 2) (program, out : output)
              where out = getValue (head modes) (pos+1)
          JumpIf f -> runProgram input step (program, output)
              where step = if cond then out else pos+3
                    cond = f tested
                    tested = getValue (head modes) (pos+1)
                    out = getValue (modes!!1) (pos+2)
          Halt -> prg

swap :: Int -> a -> [a] -> [a]
swap _ _ [] = []
swap n newVal (x:xs)
  | n == 0 = newVal:xs
  | otherwise = x:swap (n-1) newVal xs


digits 0 = [0]
digits n = digs n

digs :: Integral x => x -> [x]
digs 0 = []
digs x = digs (x `div` 10) ++ [x `mod` 10]
