module Day6
    (
    ) where

import Prelude hiding (lookup)
import qualified Data.Map.Strict as M
import Data.Map.Strict (Map, lookup, (!))
import Data.Maybe (fromJust, isJust)

main :: IO ()
main = do
    input <- readFile "input/input_6.txt"
    let orbits = map parseOrbit (lines input)
    print orbits

type Object = (String, [String])

parseOrbit :: String -> Object
parseOrbit st = (head split, tail split)
    where split = splitBy ')' st

splitBy delimiter = foldr f [[]] 
            where f c l@(x:xs) | c == delimiter = []:l
                             | otherwise = (c:x):xs

testOrbits = map parseOrbit
    ["COM)B", "B)C", "C)D", "D)E", "E)F", "B)G", "G)H", "D)I", "E)J", "J)K", "K)L"]

testMap :: Map String [String]
testMap = M.fromListWith (++) testOrbits
