module Day3
    ( main
    ) where

import qualified Data.Set as Set

main :: IO ()
main = do
    input <- readFile "input/input_3.txt"
    let firstWire  = fmap readInstruction $ split $ head $ lines input
    let secondWire = fmap readInstruction $ split $ last $ lines input

    let firstStops  = Set.fromList $ paths (0, 0) firstWire
    let secondStops = Set.fromList $ paths (0, 0) secondWire
    let common = Set.intersection firstStops secondStops
    let dists = Set.map dist common

    print $ minimum dists

    let firstCrawl  = crawlPaths ((0, 0), 0) firstWire
    let secondCrawl = crawlPaths ((0, 0), 0) secondWire

    let stepCounts = map (getSteps firstCrawl secondCrawl) (Set.toList common)

    print $ minimum stepCounts

getSteps :: [AugmentedCoord] -> [AugmentedCoord] -> Coord -> Int
getSteps set1 set2 coord =
    get set1 coord + get set2 coord
        where get (c:cs) coord
                | coord == fst c = snd c
                | otherwise      = get cs coord

readInstruction :: String -> Instruction
readInstruction cs = (toDirection $ head cs, read (tail cs) :: Int)

toDirection :: Char -> Direction
toDirection 'U' = North
toDirection 'D' = South
toDirection 'R' = East
toDirection 'L' = West

dist :: (Int, Int) -> Int
dist (x, y) = abs x + abs y

data Direction = North | South | West | East
    deriving (Eq, Show)

type Instruction = (Direction, Int)

type Coord = (Int, Int)

type AugmentedCoord = (Coord, Int)

changeCoord :: Coord -> Instruction -> Coord
changeCoord (x, y) (North, amount) = (x, y + amount)
changeCoord (x, y) (South, amount) = (x, y - amount)
changeCoord (x, y) (West, amount)  = (x - amount, y)
changeCoord (x, y) (East, amount)  = (x + amount, y)

stops :: Coord -> [Instruction] -> [Coord]
stops coord (a:as) = newCoord : stops newCoord as
    where newCoord = changeCoord coord a
stops _ [] = []

paths :: Coord -> [Instruction] -> [Coord]
paths coord@(ox, oy) (a:as)
  | fst a == East  = [(x, y) | x <- [ox + 1 .. fst newCoord], y <- [oy]] ++ paths newCoord as
  | fst a == West  = [(x, y) | x <- [ox -1, ox -2 .. fst newCoord], y <- [oy]] ++ paths newCoord as
  | fst a == North = [(x, y) | y <- [oy + 1 .. snd newCoord], x <- [ox]] ++ paths newCoord as
  | fst a == South = [(x, y) | y <- [oy -1, oy -2 .. snd newCoord], x <- [ox]] ++ paths newCoord as
  | otherwise = []
    where newCoord = changeCoord coord a
paths _ [] = []

crawlToCoord :: AugmentedCoord -> Instruction -> [AugmentedCoord]
crawlToCoord coord (dir, amount)
  | newCoord /= fst coord = advance dir coord : 
      crawlToCoord (advance dir coord) (dir, amount - 1)
  | otherwise = []
    where newCoord = changeCoord (fst coord) (dir, amount)
          advance North ((x, y), d) = ((x, y+1), d+1)
          advance East ((x, y), d)  = ((x+1, y), d+1)
          advance South ((x, y), d) = ((x, y-1), d+1)
          advance West ((x, y), d)  = ((x-1, y), d+1)

crawlPaths :: AugmentedCoord -> [Instruction] -> [AugmentedCoord]
crawlPaths coord (i:is) = crawlToCoord coord i ++ crawlPaths newCoord is
    where newCoord = last (crawlToCoord coord i)
crawlPaths _ [] = []

-- below: code from https://improve-future.com/en/haskell-split-string-into-list.html

split :: String -> [String]
split = split' []
split' :: [String] -> String -> [String]
split' ts "" = ts
split' ts s = split' (ts ++ [token s]) (drop (length(token s) + 1) s)
 
token :: String -> String
token = token' ""
token' :: String -> String -> String
token' ys "" = ys
token' ys (x:xs) = if x == ','
                      then ys
                      else token' (ys ++ [x]) xs
