module Day2
    ( main
    , processCode
    ) where

import Paths_aoc2019
import Data.List.Split

main :: IO ()
main = do
    input <- getDataFileName "input_2.txt"
    program <- readFile input
    let intCode = map read (splitOn "," program) :: [Int]
    let processed = runProgram intCode 12 2
    print $ head processed
    let gravAssist = head
            [(noun, verb) | noun <- [1..100],
                            verb <- [1..100],
                            head (runProgram intCode noun verb) == 19690720]
    print $ 100 * fst gravAssist + snd gravAssist

type Intcode = [Int]

runProgram :: Intcode -> Int -> Int -> Intcode
runProgram input noun verb = processCode 0 $ replaceNth 1 noun (replaceNth 2 verb input)

processCode :: Int -> Intcode -> Intcode
processCode pos code
  | code !! pos == 1  = processCode (pos + 4) (replaceNth output (input1 + input2) code)
  | code !! pos == 2  = processCode (pos + 4) (replaceNth output (input1 * input2) code)
  | code !! pos == 99 = code
  | otherwise = code
  where
      input1 = code!!(code!!(pos+1))
      input2 = code!!(code!!(pos+2))
      output = code!!(pos+3)

replaceNth :: Int -> a -> [a] -> [a]
replaceNth _ _ [] = []
replaceNth n newVal (x:xs)
  | n == 0 = newVal:xs
  | otherwise = x:replaceNth (n-1) newVal xs

-- noun: address 1; verb: address 2
