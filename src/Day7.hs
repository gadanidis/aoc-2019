module Day7
    ( main
    ) where

import Data.List (permutations)

main :: IO ()
main = do
    print (maximum $ map (`test` software) toTest)
    print (maximum $ map (`test` software) toTest')

toTest :: [[Int]]
toTest = permutations [0, 1, 2, 3, 4]

toTest' :: [[Int]]
toTest' = permutations [5, 6, 7, 8, 9]

test :: [Int] -> Program -> Int
test [a, b, c, d, e] program =
    thrusterValue $ runIt [ e
                          , thrusterValue $ runIt [ d
                          , thrusterValue $ runIt [ c
                          , thrusterValue $ runIt [ b
                          , thrusterValue $ runIt [ a, 0] program ] program ] program] program] program
    where thrusterValue = head . snd
test _ _ = error "bad setting"

type Program = [Int]

data Opcode = Arithmetic (Int -> Int -> Int)
            | Scan
            | Print
            | Halt
            | JumpIf (Int -> Bool)

type Input = Int

type Output = Int

data Mode = Position
          | Immediate

readOpstring :: Int -> (Opcode, [Mode])
readOpstring n = case length (digits n :: [Int]) of
                   1 -> (readOpcode op, replicate 3 Position)
                   2 -> (readOpcode op, replicate 3 Position)
                   3 -> (readOpcode op, readMode (head mo) : replicate 2 Position)
                   4 -> (readOpcode op, map readMode mo ++ [Position])
                   5 -> (readOpcode op, map readMode mo)
                where op = rem n 100
                      mo = drop 2 $ reverse $ digits n
                 

readMode :: Int -> Mode
readMode n = case n of
               0 -> Position
               1 -> Immediate
               e -> error $ "bad mode: " ++ show e

readOpcode :: Int -> Opcode
readOpcode n = case n of
                1 -> Arithmetic (+)
                2 -> Arithmetic (*)
                3 -> Scan
                4 -> Print
                5 -> JumpIf (/= 0)
                6 -> JumpIf (== 0)
                7 -> Arithmetic (\x y -> if x < y then 1 else 0)
                8 -> Arithmetic (\x y -> if x == y then 1 else 0)
                99 -> Halt
                e -> error $ "bad opcode: " ++ show e

-- must handle multiple inputs
runIt :: [Input] -> Program -> (Program, [Output])
runIt inputs prg = runProgram inputs 0 (prg, [])

runProgram :: [Input] -> Int -> (Program, [Output]) -> (Program, [Output])
runProgram inputs pos prg =
    let program = fst prg
        output = snd prg
        code  = fst $ readOpstring (program!!pos)
        modes = snd $ readOpstring (program!!pos)
        getValue Position index = program!!(program!!index)
        getValue Immediate index = (program!!index)
     in
        case code of
          Arithmetic f -> runProgram inputs (pos + 4) (swap out res program, output)
              where out = program!!(pos+3)
                    in1 = getValue (head modes) (pos+1)
                    in2 = getValue (modes!!1) (pos+2)
                    res = in1 `f` in2

          Scan -> runProgram (drop 1 inputs) (pos + 2) (swap out input program, output)
              where out = program!!(pos+1)
                    input = head inputs

          Print -> runProgram inputs (pos + 2) (program, out : output)
              where out = getValue (head modes) (pos+1)

          JumpIf f -> runProgram inputs step (program, output)
              where step   = if f tested then out else pos+3
                    tested = getValue (head modes) (pos+1)
                    out    = getValue (modes!!1) (pos+2)

          Halt -> prg

swap :: Int -> a -> [a] -> [a]
swap _ _ [] = []
swap n newVal (x:xs)
  | n == 0 = newVal:xs
  | otherwise = x:swap (n-1) newVal xs


digits 0 = [0]
digits n = digs n

digs :: Integral x => x -> [x]
digs 0 = []
digs x = digs (x `div` 10) ++ [x `mod` 10]

software :: Program
software =
    [3, 8, 1001, 8, 10, 8, 105, 1, 0, 0, 21, 38, 63, 88, 97, 118, 199, 280, 361,
    442, 99999, 3, 9, 1002, 9, 3, 9, 101, 2, 9, 9, 1002, 9, 4, 9, 4, 9, 99, 3,
    9, 101, 3, 9, 9, 102, 5, 9, 9, 101, 3, 9, 9, 1002, 9, 3, 9, 101, 3, 9, 9, 4,
    9, 99, 3, 9, 1002, 9, 2, 9, 1001, 9, 3, 9, 102, 3, 9, 9, 101, 2, 9, 9, 1002,
    9, 4, 9, 4, 9, 99, 3, 9, 102, 2, 9, 9, 4, 9, 99, 3, 9, 102, 4, 9, 9, 101, 5,
    9, 9, 102, 2, 9, 9, 101, 5, 9, 9, 4, 9, 99, 3, 9, 1002, 9, 2, 9, 4, 9, 3, 9,
    101, 1, 9, 9, 4, 9, 3, 9, 102, 2, 9, 9, 4, 9, 3, 9, 101, 1, 9, 9, 4, 9, 3,
    9, 101, 2, 9, 9, 4, 9, 3, 9, 1001, 9, 2, 9, 4, 9, 3, 9, 102, 2, 9, 9, 4, 9,
    3, 9, 102, 2, 9, 9, 4, 9, 3, 9, 101, 1, 9, 9, 4, 9, 3, 9, 102, 2, 9, 9, 4,
    9, 99, 3, 9, 101, 1, 9, 9, 4, 9, 3, 9, 102, 2, 9, 9, 4, 9, 3, 9, 1001, 9, 2,
    9, 4, 9, 3, 9, 1001, 9, 2, 9, 4, 9, 3, 9, 1001, 9, 1, 9, 4, 9, 3, 9, 1001,
    9, 1, 9, 4, 9, 3, 9, 1001, 9, 2, 9, 4, 9, 3, 9, 102, 2, 9, 9, 4, 9, 3, 9,
    1002, 9, 2, 9, 4, 9, 3, 9, 1001, 9, 1, 9, 4, 9, 99, 3, 9, 1002, 9, 2, 9, 4,
    9, 3, 9, 1002, 9, 2, 9, 4, 9, 3, 9, 102, 2, 9, 9, 4, 9, 3, 9, 1002, 9, 2, 9,
    4, 9, 3, 9, 1001, 9, 1, 9, 4, 9, 3, 9, 102, 2, 9, 9, 4, 9, 3, 9, 102, 2, 9,
    9, 4, 9, 3, 9, 101, 1, 9, 9, 4, 9, 3, 9, 102, 2, 9, 9, 4, 9, 3, 9, 102, 2,
    9, 9, 4, 9, 99, 3, 9, 102, 2, 9, 9, 4, 9, 3, 9, 101, 1, 9, 9, 4, 9, 3, 9,
    1002, 9, 2, 9, 4, 9, 3, 9, 1002, 9, 2, 9, 4, 9, 3, 9, 102, 2, 9, 9, 4, 9, 3,
    9, 1002, 9, 2, 9, 4, 9, 3, 9, 1001, 9, 2, 9, 4, 9, 3, 9, 101, 2, 9, 9, 4, 9,
    3, 9, 1001, 9, 2, 9, 4, 9, 3, 9, 101, 1, 9, 9, 4, 9, 99, 3, 9, 101, 1, 9, 9,
    4, 9, 3, 9, 101, 1, 9, 9, 4, 9, 3, 9, 101, 1, 9, 9, 4, 9, 3, 9, 102, 2, 9,
    9, 4, 9, 3, 9, 1001, 9, 1, 9, 4, 9, 3, 9, 1001, 9, 2, 9, 4, 9, 3, 9, 101, 2,
    9, 9, 4, 9, 3, 9, 102, 2, 9, 9, 4, 9, 3, 9, 1001, 9, 1, 9, 4, 9, 3, 9, 1001,
    9, 2, 9, 4, 9, 99]


tests :: IO ()
tests = do
    putStrLn "Position mode checks:"
    print $ getOutput (runIt [8] [3,9,8,9,10,9,4,9,99,-1,8]) == 1
    print $ getOutput (runIt [7] [3,9,8,9,10,9,4,9,99,-1,8]) == 0
    print $ getOutput (runIt [7] [3,9,7,9,10,9,4,9,99,-1,8]) == 1
    print $ getOutput (runIt [9] [3,9,7,9,10,9,4,9,99,-1,8]) == 0

    putStrLn "Immediate mode checks:"
    print $ getOutput (runIt [8] [3,3,1108,-1,8,3,4,3,99]) == 1
    print $ getOutput (runIt [7] [3,3,1108,-1,8,3,4,3,99]) == 0
    print $ getOutput (runIt [7] [3,3,1107,-1,8,3,4,3,99]) == 1
    print $ getOutput (runIt [9] [3,3,1107,-1,8,3,4,3,99]) == 0

    putStrLn "Position jumptest:"
    print $ getOutput (runIt [1] [3,12,6,12,15,1,13,14,13,4,13,99,-1,0,1,9]) == 1
    print $ getOutput (runIt [0] [3,12,6,12,15,1,13,14,13,4,13,99,-1,0,1,9]) == 0

    putStrLn "Immediate jumptest:"
    print $ getOutput (runIt [1] [3,3,1105,-1,9,1101,0,0,12,4,12,99,1]) == 1
    print $ getOutput (runIt [0] [3,3,1105,-1,9,1101,0,0,12,4,12,99,1]) == 0

        where getOutput = head . snd
