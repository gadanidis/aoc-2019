module Day1
    ( main
    ) where

import Paths_aoc2019

main :: IO ()
main = do
    input <- getDataFileName "input_1.txt"
    masses <- readFile input
    print $ fuelRequirement (map read $ lines masses)
    print $ fuelRequirement' (map read $ lines masses)

getFuel :: Integer -> Integer
getFuel mass = (mass `div` 3) - 2

getFuel' :: Integer -> Integer
getFuel' mass
  | getFuel mass <= 0 = 0
  | otherwise = getFuel mass + getFuel' (getFuel mass)

fuelRequirement :: [Integer] -> Integer
fuelRequirement masses = sum (map getFuel masses)

fuelRequirement' :: [Integer] -> Integer
fuelRequirement' masses = sum (map getFuel' masses)
